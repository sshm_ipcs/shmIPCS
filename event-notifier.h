#ifndef CEVENTNOTIFIER_H
#define CEVENTNOTIFIER_H

#include <unistd.h>
#include <errno.h>
#include <sys/eventfd.h>
#include <fcntl.h>



typedef int evInt;

typedef struct EventNotifier {
    evInt rfd;
    evInt wfd;
} EventNotifier;


class CEventNotifier {

public:
    CEventNotifier();

    static void event_notifier_init_fd (EventNotifier *e, evInt fd);
    static int event_notifier_set (EventNotifier *e) ;
    static int fcntl_setfl(evInt fd, int flag);
    static int event_notifier_init (EventNotifier *e, int active);
    static int event_notifier_cleanup (EventNotifier *e);
    static evInt event_notifier_get_fd(const EventNotifier *e);

};

#endif // CEVENTNOTIFIER_H
