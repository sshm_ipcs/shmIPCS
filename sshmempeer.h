#ifndef SSHMEMPEER_H
#define SSHMEMPEER_H

#include <QList>


#define PEER_VEC_SIZE 64


template <typename T>
class SSHMemPeer: public QList<T>  {

public:
    SSHMemPeer() : QList<T> () {}

    virtual int Is_ID_used (quint64 peer_id) = 0;

protected:
   // virtual sshMemPeerList<T> *getPeerFromID (int64_t  peer_id) = 0;
};

#endif // SSHMEMPEER_H
