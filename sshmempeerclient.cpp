#include "sshmempeerclient.h"

SSHMemPeerClient::SSHMemPeerClient() :SSHMemPeerCommon () {

}


int SSHMemPeerClient::addClient (qintptr sock_fd, qint64 id) {
    sshMemClientPeer new_peer;
    InitNewPeer (&new_peer, sock_fd, id);
    SSHMemPeer::append(new_peer);
    return SSHMemPeer::count ();
}


/* Implementation of static methods */

int SSHMemPeerClient::open_msg_own_port (PosixMQ *mq) {
    /*  For the Client, the opening queue must be
     *  an existing queue.
     *  The send/receive operations must be non blocking.
     *  The Client must only read from the mq reserved to it.
     */
    int flag = O_RDONLY | O_NONBLOCK;
    mode_t perms = (mode_t)0;

    return SSHMemPeerCommon::open_msg_port (mq, flag, perms);
}


int SSHMemPeerClient::open_msg_remote_port (PosixMQ *mq) {
    /*  For the Client, the opening queue must be
     *  an existing queue.
     *  The send/receive operations must be non blocking.
     *  The Client must only write to the mq of other clients.
     */
    int flag = O_WRONLY | O_NONBLOCK;
    mode_t perms = (mode_t)0;

    return SSHMemPeerCommon::open_msg_port (mq, flag, perms);
}


int SSHMemPeerClient::close_msg_port (PosixMQ *mq) {
    /*  The Client must only close the message queue,
     *  without unlink it.
     */
    return mq->close ();
}
