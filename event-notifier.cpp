#include "event-notifier.h"

CEventNotifier::CEventNotifier()
{

}



void CEventNotifier::event_notifier_init_fd (EventNotifier *e, evInt fd) {
    e->rfd = fd;
    e->wfd = fd;
}


int CEventNotifier::event_notifier_set (EventNotifier *e) {
    static const uint64_t value = 1;
    ssize_t ret;

    do {
        ret = write (e->rfd, &value, sizeof(value));
    } while ( ret < 0 && errno == EINTR );

    if ( ret < 0 && errno != EAGAIN )
        return -errno;
    return 0;
}


int CEventNotifier::fcntl_setfl(evInt fd, int flag) {
    int flags;
    flags = fcntl (fd, F_GETFL);
    if ( flags == -1 )
        return -errno;
    if ( fcntl (fd, F_SETFL, flags | flag) == -1 )
        return -errno;
    return 0;
}


int CEventNotifier::event_notifier_init (EventNotifier *e, int active) {
    int fds[2];
    int ret, pret;

    ret = eventfd (0, EFD_NONBLOCK | EFD_CLOEXEC);
    if ( ret >= 0 ) {

       e->rfd = (evInt)ret;
       e->wfd = (evInt)ret;

    } else {

        if ( errno != ENOSYS ) {
            goto fail1;
        }

        pret = pipe (fds);
        if ( pret == 0 ) {
            fcntl (fds[0], F_SETFD, fcntl(fds[0], F_GETFD) | FD_CLOEXEC);
            fcntl (fds[1], F_SETFD, fcntl(fds[1], F_GETFD) | FD_CLOEXEC);
        } else if ( pret < 0 ) {
            goto fail1;
        }

        ret = fcntl_setfl(fds[0], O_NONBLOCK);
        if ( ret < 0 ) {
            goto fail2;
        }
        ret = fcntl_setfl(fds[1], O_NONBLOCK);
        if ( ret < 0 ) {
            goto fail2;
        }

        e->rfd = fds[0];
        e->wfd = fds[1];

    }

    if ( active ) {
        event_notifier_set (e);
    }

    return 0;
fail2:
    close(fds[0]);
    close(fds[1]);
fail1:
    return -errno;
}


int CEventNotifier::event_notifier_cleanup (EventNotifier *e) {
    if ( e->rfd != e->wfd )
        close ((int)e->rfd);
    close ((int)e->wfd);
    return 0;
}


evInt CEventNotifier::event_notifier_get_fd (const EventNotifier *e) {
    return e->rfd;
}
