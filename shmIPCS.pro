#-------------------------------------------------
#
# Project created by QtCreator 2016-02-24T22:34:36
#
#-------------------------------------------------

QT       += network

QT       -= gui

TARGET = shmIPCS
TEMPLATE = lib
CONFIG +=dynamiclib


SOURCES += \
    event-notifier.cpp \
    sshmempeer.cpp \
    sshmempeerclient.cpp \
    sshmempeerserver.cpp \
    sshmempeercommon.cpp \
    sshmemclient.cpp \
    sshmemserver.cpp \
    sshmem.cpp \
    posix_mq.cpp

HEADERS += \
    event-notifier.h \
    sshmempeer.h \
    sshmempeerclient.h \
    sshmempeerserver.h \
    sshmempeercommon.h \
    sshmemclient.h \
    sshmemserver.h \
    shm_ipcs.h \
    sshmem.h \
    posix_mq.h

unix {
    #target.path = /usr/lib
    target.path = /home/davide/Code/SHM_IPCS_PRO/
    target.file = .so
    INSTALLS += target
}
