#ifndef SHM_IPCS
#define SHM_IPCS

#include <QtGlobal>
#include <QVariant>
#include <QString>

#if QT_VERSION < 0x040800
    typedef quint64 qintptr;
#endif

/*  Socket Message Coding  */
enum msg_sock_code {
    MSG_SOCK_ERROR      = 0x0,
    MSG_SOCK_INIT       = 0x1,
    MSG_SOCK_EVN_INFO   = 0x2,
    MSG_SOCK_ADD_CLIENT = 0x3,
    MSG_SOCK_NEW_CLIENT = 0x4,
    MSG_SHMEM_KEY       = 0x5,
    MSG_SOCK_MQ_INFO    = 0x6
};


/*  Socket Polling Message Coding  */
enum msg_sock_polling_code {
    MSG_P_SOCK_ERROR       = 0x0,
    MSG_P_SOCK_INIT        = 0x1,
    MSG_P_SOCK_EVN_INFO    = 0x2,
    MSG_P_SOCK_ADD_CLIENT  = 0x3,
    MSG_P_SOCK_NEW_CLIENT  = 0x4,
    MSG_P_SHMEM_KEY        = 0x5,
    MSG_P_SOCK_MQ_INFO     = 0x6,
    MSG_P_MEM_ATTACH_ERROR = 0x7,
    MSG_P_GENERIC          = 0x8
};


typedef struct queue_sys_element {
    quint16         code;
    QList<QVariant> data;
}queue_sys_element_t;


enum msg_peer_cmd_code {
    MSG_PEER_PING_S     = 0x1,  // Ping from Server
    MSG_PEER_PING_C     = 0x2,  // Ping from Client
};


typedef struct queue_msg_element {
    quint64 source_id;
    quint64 line;
}queue_msg_element_t;


/*  Number of Line of Interrupt  */
const int MIN_INT_LINE          = 1;
const int MAX_INT_LINE          = 64;
/*  Number of bit for the CODE for a MQ message  */
const int MQ_CODE_SIZE          = 8;
const int MSG_SIZE_PEER_PING_S  = MQ_CODE_SIZE + 64;


const long MQ_QUEUE_DEEP        = 10;
const long MQ_SIZE              = 512;

const QString SHM_KEY = "shm_IPCS_shared_memory";


#endif // SHM_IPCS
