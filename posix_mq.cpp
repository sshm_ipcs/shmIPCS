#include <stdlib.h>
#include "posix_mq.h"

PosixMQ::PosixMQ () {

    this->mq_fd_name = "";

    this->is_open = false;
    this->queue_deep = 10;
    this->max_msg_size = 4096;
    this->flags = 0;
}


bool PosixMQ::mq_is_open () {
    return (this->is_open && (this->mqd > 0));
}


int PosixMQ::set_fd_name (QString name) {
    if ( this->mq_is_open () == false ) {
        this->mq_fd_name = name;
        return 0;
    }
    return -1;
}


QString PosixMQ::get_fd_name () {
    return this->mq_fd_name;
}


int PosixMQ::set_msg_size (long size) {
    if ( this->mq_is_open () == false ) {
        this->max_msg_size = size;
        return 0;
    }
    return -1;
}


long PosixMQ::get_msg_size () {
    return this->max_msg_size;
}


int PosixMQ::set_queue_deep (long deep) {
    if ( this->mq_is_open () == false ) {
        this->queue_deep = deep;
        return 0;
    }
    return -1;
}


long PosixMQ::get_queue_deep () {
    return this->queue_deep;
}


mq_attr *PosixMQ::get_attrs () {
    int ret = -1;
    mq_attr *attr = new mq_attr;
    if ( this->mq_is_open () ) {
        ret = mq_getattr (this->mqd, attr);
    }
    if ( ret == 0 )
        return attr;
    else
        return NULL;
}


long PosixMQ::get_attr_flags () {
    mq_attr *attr = this->get_attrs ();
    if ( attr )
        return attr->mq_flags;
    else
        return (long)-1;
}


long PosixMQ::get_attr_max_num_msg () {
    mq_attr *attr = this->get_attrs ();
    if ( attr )
        return attr->mq_maxmsg;
    else
        return (long)-1;
}


long  PosixMQ::get_attr_max_msg_size () {
    mq_attr *attr = this->get_attrs ();
    if ( attr )
        return attr->mq_msgsize;
    else
        return (long)-1;
}


long PosixMQ::get_attr_msgs_in_queue () {
    mq_attr *attr = this->get_attrs ();
    if ( attr )
        return attr->mq_curmsgs;
    else
        return (long)-1;
}


int PosixMQ::set_attr_flags (long flags) {
    return 0;
}


int PosixMQ::msg_to_read () {
    if ( this->mq_is_open () )
        return get_attr_msgs_in_queue ();
    else
        return 0;
}


int PosixMQ::open (int flag, mode_t perms) {
    mq_attr attr;
    const char *name = this->mq_fd_name.toUtf8().constData();

    if ( this->mq_is_open () ) {
        return -1;
    }

    if ( perms != 0 ) {
        attr.mq_maxmsg = this->queue_deep;
        attr.mq_msgsize = this->max_msg_size;

        this->mqd = mq_open (name, flag, perms, &attr);
    } else {
        this->mqd = mq_open (name, flag);
    }

    if ( mqd != (mqd_t)-1) {
        this->flags = flag;
        this->is_open = true;
    }

    return this->mqd != (mqd_t)-1 ? 0 : errno;
}


int PosixMQ::send (QString message) {
    int ret = 0;

    if ( !this->mq_is_open () ) {
        return -1;
    }

    ret = mq_send (this->mqd,
                   message.toUtf8().constData(),
                   message.length(),
                   0);

    return ret == 0 ? ret : errno;
}


int PosixMQ::send (QByteArray message) {
    int ret = 0;

    if ( !this->mq_is_open () ) {
        return -1;
    }

    ret = mq_send (this->mqd,
                   message.constData(),
                   message.length(),
                   0);

    return ret == 0 ? ret : errno;
}


int PosixMQ::receive (QByteArray *message) {
    int          ret = 0;
    mq_attr      *attr;
    char         *buffer;
    //char buffer[520];

    if ( !this->mq_is_open () ) {
        return -1;
    }

    attr = this->get_attrs ();
    buffer = (char *)malloc (sizeof (char) * (attr->mq_msgsize+1));
    if ( buffer == NULL )
        return ENOMEM;

    ret = mq_receive (this->mqd,
                      &buffer[0],
                      attr->mq_msgsize + 1,
                      NULL);

    if ( ret > 0 ) {
        *message = QByteArray (buffer, attr->mq_msgsize + 1);

    }

    return ret > 0 ? 0 : errno;
}


int PosixMQ::close () {
    int ret = 0;

    if ( this->mq_is_open () ) {
        ret =  mq_close (this->mqd);
        if ( ret == 0 )
            this->is_open = false;
        return ret == 0 ? ret : errno;
    } else
        return -1;
}


int PosixMQ::unlink () {
    int ret = mq_unlink (this->mq_fd_name.toUtf8().constData());
    if ( ret == 0 )
        this->is_open = false;
    return ret == 0 ? ret : errno;
}
