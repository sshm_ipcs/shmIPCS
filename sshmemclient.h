#ifndef SSHMEMCLIENT_H
#define SSHMEMCLIENT_H

#include <QObject>
#include <QThread>
#include <qlocalsocket.h>
#include <QIODevice>
#include <QByteArray>
#include <QDataStream>
#include <QQueue>

#include <cstdarg>

#include "shm_ipcs.h"
#include "sshmem.h"
#include "sshmempeerclient.h"

QT_BEGIN_NAMESPACE
class QLocalSocket;
QT_END_NAMESPACE


class SSHMemClient : public QThread, public sshMem {
    Q_OBJECT

void run ();

public:
/*  Costructor / Destructor  */
    explicit SSHMemClient(QString sockFileName, quint64 shm_size = 0,
                          QString shm_key = SHM_KEY,  bool usePolling = false, QThread *parent = 0);
    explicit SSHMemClient(quint64 shm_size = 0, bool usePolling = false, QThread *parent = 0);
    ~SSHMemClient ();

/*  Attribute Interface  */
    QString getSocketFileName ();
    int setSocketFileName (QString FileName);
    const sshMemClientPeer getLocalPeer ();
    const SSHMemPeerClient getRemotePeerList ();
    quint64 getClientID ();
    qintptr getSockID ();

/*  Socket Management  */
    int ConnectClient ();
    int DisconnectClient ();

    int isClientConnected ();

    bool dataReady ();

    bool isInitialized ();
    bool isShmInitialized ();
    bool isValidSockID (qintptr socket_id);
    bool isValidClientID (quint64 client_id);

/*  Peer Management (via MQ)  */
    int send_PingToClient (int clientID, int line);

/*  Event Polling Management  */
    int num_pending_message ();
    int num_pending_sys_msg ();
    int pop_message (queue_msg_element_t *msg);
    int pop_sys_msg (queue_sys_element_t *msg);

signals:
    void clientInitialized (qintptr sock_id, int client_id);
    void clientPeerInitialized (QString mq_name);
    void clientPeerConnectionError (int err);
    void newMessage (QString msg);
    void newClientConnected (qintptr sock_id, int client_id);
    void clientUpdateInterconnection ();
    void sharedMemoryAttached (QString key, int size);
    void sharedMemoryAttachError (int err);

    void PingReceived (qint64 sourceID, int line);

public slots:


private slots:
    void _readMessage ();


protected:
/*  Socket Management  */
    bool manageMsg_InitStage (QDataStream *in);
    bool manageMsg_InitPeer (QDataStream *in);
    bool manageMsg_InfoPeer (QDataStream *in, sshMemClientPeer *other_peer);
    bool manageMsg_NewClient (QDataStream *in, sshMemClientPeer *new_client);
    bool manageMsg_ShmKey (QDataStream *in);
/*  Message Management  */
    int analyzesMsg (QByteArray msg);
    int manageMsg_ReceivedPingServer (QByteArray msg);
    int manageMsg_ReceivedPingClient (QByteArray msg);
    void createMsg_PingClient (QByteArray *msg, int line);

    QLocalSocket *socket;
    QString socket_filename;

    bool is_initialized;
    bool is_shm_initialized;

    sshMemClientPeer *localPeer;
    SSHMemPeerClient *remotePeerList;

/*  Event Polling Management  */
    bool usePolling;
    QQueue<queue_sys_element_t> *queue_system;
    QQueue<queue_msg_element_t> *queue_message;



#define EMIT_EVENT(ev, arg...)         emit ev(## arg)


};

#endif // SSHMEMCLIENT_H
