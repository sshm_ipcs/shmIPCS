

#include "sshmem.h"

sshMem::sshMem (bool server) {
    this->shm_key = "";
    this->shm_initialized = false;
    this->shm_is_server = server;
    this->shm_size = 0;
    this->sharedMemory = NULL;
}


sshMem::sshMem (QString key, bool server) {
    this->shm_key = key;
    this->shm_initialized = false;
    this->shm_is_server = server;
    this->shm_size = 0;
    this->sharedMemory = new QSharedMemory (this->shm_key);
}


sshMem::sshMem (quint64 size, QString key, bool server) {
    this->shm_key = key;
    this->shm_size = size;
    this->shm_initialized = false;
    this->shm_is_server = server;
    this->sharedMemory = new QSharedMemory (this->shm_key);


    if ( ! this->sharedMemory == NULL && size != 0 ) {

        if ( this->shm_is_server == true ) {
            if ( this->sharedMemory->create (size) )
                this->shm_initialized = true;
        } else {
            if ( this->sharedMemory->attach () )
                this->shm_initialized = true;
        }

    }
}


int sshMem::createSHM () {
    if ( this->sharedMemory == NULL )
        this->sharedMemory = new QSharedMemory (this->shm_key);

    if ( ( this->sharedMemory != NULL) && this->shm_size != 0 ) {

        if ( this->shm_is_server == true ) {
            if ( this->sharedMemory->create (this->shm_size) ) {
                this->shm_initialized = true;
            } else {
                return this->sharedMemory->error ();
            }
        } else {
            return -1;
        }
    }

    return 0;
}


int sshMem::attachSHM () {
    if ( this->sharedMemory == NULL )
        this->sharedMemory = new QSharedMemory (this->shm_key);

    if ( this->sharedMemory != NULL) {
        if ( this->shm_is_server == false ) {
            if ( this->sharedMemory->isAttached() )
                this->sharedMemory->detach ();
            if ( this->sharedMemory->attach () ) {
                this->shm_size = this->sharedMemory->size ();
                this->shm_initialized = true;
            } else {
                return this->sharedMemory->error ();
            }
        } else {
            return -1;
        }
    }

    return 0;
}


void sshMem::setMemSize(quint64 size) {
    this->shm_size = size;
}


quint64 sshMem::getMemSize () {
    return this->sharedMemory->size ();
}


void sshMem::setMemKey (QString key) {
    this->shm_key = key;
}


QString sshMem::getMemKey () {
    return this->shm_key;
}


QString sshMem::getMsgFromErrCode (int code) {
    QString err;
    switch (code) {
    case 0:
        err = "No error";
        break;
    case 1:
        err = "Permission Denied";
        break;
    case 2:
        err = "Invalid size";
        break;
    case 3:
        err = "Key error";
        break;
    case 4:
        err = "Already exists";
        break;
    case 5:
        err = "Not found";
        break;
    case 6:
        err = "Lock error";
        break;
    case 7:
        err = "Out of resources";
        break;
    case 8:
    default:
        err = "Unknown error";
        break;
    }
}

int sshMem::flash_memory_from_file (QString fileName) {
    int size;
    QBuffer *buffer;
    QByteArray content;
    QFile file (fileName);

    if ( this->shm_initialized == false ) {
        return -1;
    }

    if ( QFile::exists (fileName) == false ) {
        return -2;
    }

    file.open (QIODevice::ReadOnly);
    content = file.readAll ();
    file.close ();

    buffer = new  QBuffer (&content);
    buffer->open (QBuffer::ReadWrite);

    size = buffer->size ();

    this->sharedMemory->lock ();
    char *to = (char*)this->sharedMemory->data ();
    const char *from = buffer->data().data();
    memcpy (to, from, qMin(this->sharedMemory->size(), size));
    this->sharedMemory->unlock ();

    return size;
}



int sshMem::dump_memory_to_file (QString fileName) {
     QBuffer buffer;
    QByteArray content;
    QFile file (fileName);

    if ( this->shm_initialized == false ) {
        return -1;
    }

    this->sharedMemory->lock ();

    buffer.setData ((char*)this->sharedMemory->constData(), this->sharedMemory->size());
    buffer.open (QBuffer::ReadOnly);
    content = buffer.buffer ();
    buffer.close ();

    file.open (QIODevice::WriteOnly);
    file.write (content);
    file.close ();

    this->sharedMemory->unlock();
}


