#ifndef SSHMEMPEERCLIENT_H
#define SSHMEMPEERCLIENT_H

#include <QVector>
#include "sshmempeer.h"
#include "sshmempeercommon.h"
#include "event-notifier.h"
#include "shm_ipcs.h"


typedef sshMemPeer sshMemClientPeer;


class SSHMemPeerClient : public SSHMemPeerCommon {

public:
    SSHMemPeerClient();

    int addClient (qintptr sock_fd, qint64 id);

public:
    static int open_msg_own_port (PosixMQ *mq);
    static int open_msg_remote_port (PosixMQ *mq);
    static int close_msg_port (PosixMQ *mq);
};


#endif // SSHMEMPEERCLIENT_H
