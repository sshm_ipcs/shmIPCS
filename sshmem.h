#ifndef SSHMEM_H
#define SSHMEM_H

#include <QString>
#include <QFile>
#include <QBuffer>
#include <qsharedmemory.h>

#include "shm_ipcs.h"


class sshMem {

public:
    sshMem (bool server);
    sshMem (QString key, bool server);
    sshMem (quint64 size, QString key, bool server);

    void setMemSize (quint64 size);
    quint64 getMemSize ();

    void setMemKey (QString key);
    QString getMemKey ();

    int flash_memory_from_file (QString fileName);
    int dump_memory_to_file (QString fileName);

    QString getMsgFromErrCode (int code);

protected:
    int createSHM ();
    int attachSHM ();

    QSharedMemory *sharedMemory;
    QString shm_key;
    bool    shm_is_server;
    bool    shm_initialized;
    quint64 shm_size;

};

#endif // SSHMEM_H
