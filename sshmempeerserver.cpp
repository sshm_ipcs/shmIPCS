#include "sshmempeerserver.h"

SSHMemPeerServer::SSHMemPeerServer() :SSHMemPeerCommon () {

}


int SSHMemPeerServer::addClient (qintptr sock_fd, quint64 id) {
    sshMemServerPeer new_peer;
    InitNewPeer (&new_peer, sock_fd, id);
    SSHMemPeer::append(new_peer);
    return SSHMemPeer::count ();
}


/* Implementation of static methods */

int SSHMemPeerServer::open_msg_port (PosixMQ *mq) {
    /*  For the Server, the opening queue must be not
     *  an existing queue and must be exclusive.
     *  The send/receive operations must be non blocking.
     */
    int flag = O_CREAT | O_EXCL | O_RDWR | O_NONBLOCK;
    mode_t perms = S_IRUSR | S_IWUSR;

    return SSHMemPeerCommon::open_msg_port (mq, flag, perms);
}


int SSHMemPeerServer::close_msg_port (PosixMQ *mq) {
    /*  The Server must delete the message queue.  */
    return mq->unlink ();
}


int SSHMemPeerServer::close_all_msg_port () {
    SSHMemPeerServer::iterator iter;
    int closed = 0;
    for ( iter = this->begin() ; iter != this->end() ; ++iter ) {
        if ( iter->message_port->unlink () == 0 )
            closed++;

    }
    return closed;
}
