#include "sshmemserver.h"

/*  ___________________________________________________________________
 * |                                                                   |
 * |                     COSTRUCTOR / DESTRUCTOR                       |
 * |___________________________________________________________________|
 */
SSHMemServer::SSHMemServer(quint64 shm_size, QThread *parent) :
                           QThread(parent), sshMem(true) {
    this->server = new QLocalServer (this);
    this->numClientConnected = 0;
    this->cur_id = 1; //0;
    this->peerList = new SSHMemPeerServer ();
    connect (server, SIGNAL(newConnection()), this, SLOT(_handleNewConnection()));
}


SSHMemServer::SSHMemServer(QString sockFileName, quint64 shm_size, QString shm_key,
                           QThread *parent) : QThread(parent), sshMem(shm_size, shm_key, true) {
    this->server = new QLocalServer (this);
    this->numClientConnected = 0;
    this->cur_id = 0;
    connect (server, SIGNAL(newConnection()), this, SLOT(_handleNewConnection()));
    this->socket_filename = sockFileName;
}


SSHMemServer::~SSHMemServer () {
    if ( shm_initialized )
        this->sharedMemory->detach ();
}


void SSHMemServer::run () {
    while (1) {
        msleep (100);
    }
}


/*  ___________________________________________________________________
 * |                                                                   |
 * |                        ATTRIBUTE INTERFACE                        |
 * |___________________________________________________________________|
 */
QString SSHMemServer::getSocketFileName () {
    return this->socket_filename;
}


int SSHMemServer::setSocketFileName (QString FileName) {
    if ( isServerConnected() ) {
        return -1;
    } else {
        socket_filename = FileName;
        return 0;
    }
}

const SSHMemPeerServer SSHMemServer::getClientStructure () {
    return  (const SSHMemPeerServer)*this->peerList;
}


/*  ___________________________________________________________________
 * |                                                                   |
 * |                          SOCKET MANAGEMENT                        |
 * |___________________________________________________________________|
 */
int SSHMemServer::ConnectServer () {
    int ret = 0;

    if ( this->socket_filename.isEmpty() )
        return -1;

    if ( this->server->listen(this->socket_filename) == true ) {

        if ( this->shm_initialized == false )
            ret = createSHM ();

    } else
        return -1;

    return ret;
}


int SSHMemServer::DisconnectServer () {
    this->server->close ();
    this->numClientConnected = 0;
    this->cur_id = 0;
    this->peerList->close_all_msg_port ();
    this->peerList->clear();

    if ( shm_initialized )
        this->sharedMemory->detach ();
}


int SSHMemServer::isServerConnected () {
    return this->server->isListening ();
}


int SSHMemServer::getNumClientConnected () {
    return numClientConnected;
}


quint64 SSHMemServer::getNewPeerID() {
    while ( this->peerList->Is_ID_used (this->cur_id) ) {
        this->cur_id++;
    }

    return this->cur_id;
}


void SSHMemServer::createMsg_InitClient (QByteArray *msg,
                           qintptr sock_id, quint64 client_id) {

    QDataStream data(msg, QIODevice::WriteOnly);
    data.setVersion (QDataStream::Qt_4_0);

    data << client_id;
    data << sock_id;

}


void SSHMemServer::createMsg_MQ_fd_name (QByteArray *msg, PosixMQ *mq) {

    QDataStream data(msg, QIODevice::WriteOnly);
    data.setVersion (QDataStream::Qt_4_0);

    data << mq->get_fd_name ();
}


void SSHMemServer::createMsg_CurrClient (QByteArray *msg, sshMemServerPeer *curr_client) {

    QDataStream data(msg, QIODevice::WriteOnly);
    data.setVersion (QDataStream::Qt_4_0);

    data << (quint64)curr_client->id;
    data << (quint64)curr_client->sock_fd;

    data << curr_client->message_port->get_fd_name();
}


void SSHMemServer::createMsg_NewClient (QByteArray *msg, sshMemServerPeer *new_client) {

    QDataStream data(msg, QIODevice::WriteOnly);
    data.setVersion (QDataStream::Qt_4_0);

    data << (quint64)new_client->id;
    data << (quint64)new_client->sock_fd;

    data << new_client->message_port->get_fd_name();
}


void SSHMemServer::createMsg_ShmKey (QByteArray *msg) {

    QDataStream data(msg, QIODevice::WriteOnly);
    data.setVersion (QDataStream::Qt_4_0);

    data << this->getMemKey ();
}


void SSHMemServer::sendMessage (QLocalSocket *clientConnection, quint16 code, QByteArray msg) {
    QByteArray block;
    QDataStream out(&block, QIODevice::WriteOnly);
    out.setVersion (QDataStream::Qt_4_0);
    out << (quint16)0;      // for message code
    out << (quint16)0;      // for message size
    out.writeRawData(msg.data(), msg.count());
    out.device()->seek(0);
    out << code;
    out << (quint16)(block.size() - sizeof(quint32));

    clientConnection->write (block);
    clientConnection->flush ();
    clientConnection->waitForBytesWritten(3000);

}



/*  ___________________________________________________________________
 * |                                                                   |
 * |                            SLOT HANDLER                           |
 * |___________________________________________________________________|
 */
void SSHMemServer::_handleNewConnection () {
    quint64                    newClientID;
    QByteArray                 msg_info, msg_mq_info, msg_curr_client,
                               msg_new_client, msg_shm_key;
    sshMemServerPeer           *new_client_peer;
    SSHMemPeerServer::iterator iter;
    QLocalSocket               *new_client;

    /*  Acquire new client  */
    new_client = this->server->nextPendingConnection ();
    qintptr new_sock_id = new_client->socketDescriptor ();
    if ( new_sock_id < 0 ) {
        // error
    }

    /*  Initialize new client and add it to the list */
    newClientID = getNewPeerID ();
    peerList->addClient (new_sock_id, newClientID);
    this->numClientConnected++;

    /*  Retrive the Peer structure about the new client  */
    new_client_peer = &(*(--peerList->end ()));

    /*  Create the message queue for this new client  */
    int ret = SSHMemPeerServer::open_msg_port (new_client_peer->message_port);

    /*  Send info about position into System Structure  */
    createMsg_InitClient (&msg_info, new_sock_id, newClientID);
    this->sendMessage (new_client, MSG_SOCK_INIT, msg_info);

    /*  Send info about the Posix Message Queue reserved for this
     *  new client
     */
    createMsg_MQ_fd_name (&msg_mq_info, new_client_peer->message_port);
    this->sendMessage (new_client, MSG_SOCK_MQ_INFO, msg_mq_info);

    /*  Infor the new client about the other present clients  */
    for ( iter = this->peerList->begin() ; iter != this->peerList->end() ; ++iter) {
        if ( iter->id != new_client_peer->id ) {
            createMsg_CurrClient (&msg_curr_client, &(*iter));
            this->sendMessage (new_client, MSG_SOCK_ADD_CLIENT, msg_curr_client);
            msg_curr_client.clear ();
        }
    }

    /*  Infor the other client about the presence of the new client
     *  into the System Structure
     */
    createMsg_NewClient (&msg_new_client, new_client_peer);
    for ( iter = this->peerList->begin() ; iter != this->peerList->end() ; ++iter) {
        QLocalSocket *client_sock;
        if ( iter->id != new_client_peer->id ) {
            client_sock = new QLocalSocket ();
            client_sock->setSocketDescriptor (iter->sock_fd);
            this->sendMessage (client_sock, MSG_SOCK_NEW_CLIENT, msg_new_client);
        }
    }

    /*  Send info about the Shared Memory to the new client  */
    createMsg_ShmKey (&msg_shm_key);
    this->sendMessage (new_client, MSG_SHMEM_KEY, msg_shm_key);

    emit newClientConnected ();
}



/*  ___________________________________________________________________
 * |                                                                   |
 * |                      PEER MANAGEMENT (VIA MQ)                     |
 * |___________________________________________________________________|
 */
void SSHMemServer::createMsg_PingClient (QByteArray *msg, int line)
{
    QDataStream data(msg, QIODevice::WriteOnly);
    data.setVersion (QDataStream::Qt_4_0);

    data << (qint8)MSG_PEER_PING_S;
    data << (qint64)(line);
}


int SSHMemServer::send_PingToClient (int clientID, int line) {
    sshMemServerPeer   *client_peer = NULL;
    int                ret          = 0;
    QByteArray         msg;

    if ( line > MAX_INT_LINE)
        return -EINVAL;

    if ( !this->isServerConnected() )
        return -1;

    client_peer = (sshMemServerPeer *)this->peerList->searchPeer (clientID);
    if ( !client_peer )
        return -1;

    this->createMsg_PingClient (&msg, line);
    ret = client_peer->message_port->send (msg);
    return ret;
}



