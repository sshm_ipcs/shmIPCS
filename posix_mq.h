#ifndef POSIX_MQ_H
#define POSIX_MQ_H


/*  PMQ - Posix Message Queue  */


#include <QString>

#include <fcntl.h>
#include <errno.h>
#include <sys/stat.h>                  /* Defines O_* constants */
#include <mqueue.h>                    /* Defines mode constants */



class PosixMQ {

public:
    PosixMQ ();

    /*  High livel Refactoring  */
    int        set_fd_name (QString name);
    QString    get_fd_name ();

    int        set_msg_size (long size);
    long       get_msg_size ();

    int        set_queue_deep (long deep);
    long       get_queue_deep ();

    int        get_used_flags ();

    int        msg_to_read ();


    /*  Posix MQ basic operation  */
    int        open (int flag, mode_t perms);
    int        send (QString message);
    int        send (QByteArray message);
    int        receive (QByteArray *message);
    int        close ();
    int        unlink ();

private:
    mqd_t      mqd;
    QString    mq_fd_name;

    bool       is_open;
    long       queue_deep;
    long       max_msg_size;
    int        flags;   // used to avoid the access to get_attr function

    bool       mq_is_open ();

    /*  Posix MQ Attributes Refactoring  */
    mq_attr   *get_attrs ();
    long       get_attr_flags ();
    int        set_attr_flags (long flags);
    long       get_attr_max_num_msg ();
    long       get_attr_max_msg_size ();
    long       get_attr_msgs_in_queue ();

};

#endif // POSIX_MQ_H
