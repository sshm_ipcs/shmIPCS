#include "sshmempeercommon.h"

SSHMemPeerCommon::SSHMemPeerCommon () {

}


int SSHMemPeerCommon::InitNewPeer (sshMemPeer *peer, qintptr sock_fd,
                                   quint64 id)
{
    return SSHMemPeerCommon::InitPeer (peer, sock_fd, id);
}


int SSHMemPeerCommon::Is_ID_used (quint64 peer_id) {

    for ( int i = 0 ; i < SSHMemPeer::size () ; ++i) {
        sshMemPeer peer = SSHMemPeer::at (i);
        if ( peer.id == peer_id ) {
            return 1;
        }
    }
    return 0;
}


sshMemPeer *SSHMemPeerCommon::searchPeer(qint64 peer_id) {

    SSHMemPeer::iterator iter;
    sshMemPeer           *peer = NULL;

    for ( iter = SSHMemPeer::begin() ; iter != SSHMemPeer::end() ; ++iter) {
        if ( iter->id == peer_id ) {
            peer = &(*iter);
        }
    }

    return peer;
}


/* static method */
int SSHMemPeerCommon::InitPeer (sshMemPeer *peer, qintptr sock_fd,
                                quint64 id)
{
    /* the name used for the message queue is:
     * /mq_<id>
     */
    QString mq_name = "" ;
    int ret = -1;

    if ( !peer ) {
        return -1;
    }
    peer->sock_fd = sock_fd;
    peer->id = id;

    peer->message_port = new PosixMQ ();
    if ( peer->message_port ) {
        mq_name.sprintf ("/mq%d", id);
        peer->message_port->set_fd_name (mq_name);
        peer->message_port->set_msg_size (MQ_SIZE);
        peer->message_port->set_queue_deep (MQ_QUEUE_DEEP);
        ret = 0;
    }

    return ret;
}


int SSHMemPeerCommon::open_msg_port (PosixMQ *mq, int flag, mode_t perms) {
    return mq->open (flag, perms);
}
