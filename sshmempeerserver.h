#ifndef SSHMEMPEERSERVER_H
#define SSHMEMPEERSERVER_H

#include <QVector>
#include "sshmempeer.h"
#include "sshmempeercommon.h"
#include "event-notifier.h"
#include "shm_ipcs.h"


typedef sshMemPeer sshMemServerPeer;


class SSHMemPeerServer : public SSHMemPeerCommon {

public:
    SSHMemPeerServer();

    int addClient (qintptr sock_fd, quint64 id);

public:
    static int open_msg_port (PosixMQ *mq);
    static int close_msg_port (PosixMQ *mq);
    int close_all_msg_port ();

};

#endif // SSHMEMPEERSERVER_H
