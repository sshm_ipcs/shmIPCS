#ifndef SSHMEMPEERCOMMON_H
#define SSHMEMPEERCOMMON_H

#include <QVector>
#include "sshmempeer.h"
#include "event-notifier.h"
#include "shm_ipcs.h"
#include "posix_mq.h"


typedef struct sshMemPeer {
    qintptr                 sock_fd;                 /*  connected unix sock */
    quint64                 id;                      /*  the id of the peer  */
    PosixMQ                 *message_port;
} sshMemPeer;



class SSHMemPeerCommon :  public SSHMemPeer<sshMemPeer> {
public:
    SSHMemPeerCommon();

    int InitNewPeer (sshMemPeer *peer,
                    qintptr sock_fd, quint64 id);

    static int InitPeer (sshMemPeer *peer,
                  qintptr sock_fd, quint64 id);

    // implemetation of inherited virtual methods
    int Is_ID_used (quint64 peer_id);
    sshMemPeer *searchPeer (qint64 peer_id);

protected:
    static int open_msg_port (PosixMQ *mq, int flag, mode_t perms);
};

#endif // SSHMEMPEERCOMMON_H
