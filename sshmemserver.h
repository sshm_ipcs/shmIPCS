#ifndef SSHMEMSERVER_H
#define SSHMEMSERVER_H

#include <QObject>

#include <qlocalserver.h>
#include <qlocalsocket.h>
#include <QIODevice>
#include <QByteArray>
#include <QDataStream>
#include <QThread>

#include "shm_ipcs.h"
#include "sshmem.h"
#include "sshmempeerserver.h"

QT_BEGIN_NAMESPACE
class QLocalServer;
QT_END_NAMESPACE


class SSHMemServer : public QThread, public sshMem {
    Q_OBJECT

    void run ();

public:
/*  Costructor / Destructor  */
    explicit SSHMemServer(QString sockFileName, quint64 shm_size = 0,
                          QString shm_key = SHM_KEY, QThread *parent = 0);
    explicit SSHMemServer(quint64 shm_size = 0, QThread *parent = 0);
    ~SSHMemServer ();

/*  Attribute Interface  */
    QString getSocketFileName ();
    int setSocketFileName (QString FileName);
    const SSHMemPeerServer getClientStructure ();

/*  Socket Management  */
    int ConnectServer ();
    int DisconnectServer ();

    int isServerConnected ();
    int getNumClientConnected ();

/*  Peer Management (via MQ)  */
    int send_PingToClient (int clientID, int line);

signals:
    void newClientConnected ();


public slots:


private slots:
    void _handleNewConnection ();


protected:
/*  Socket Management  */
    quint64 getNewPeerID();
    void createMsg_InitClient (QByteArray *msg, qintptr sock_id, quint64 peer_id);
    void createMsg_MQ_fd_name (QByteArray *msg, PosixMQ *mq);
    void createMsg_NewClient (QByteArray *msg, sshMemServerPeer *new_client);
    void createMsg_CurrClient (QByteArray *msg, sshMemServerPeer *curr_client);
    void createMsg_ShmKey (QByteArray *msg);
    void sendMessage (QLocalSocket *clientConnection, quint16 code, QByteArray msg);
/*  Message Management  */
    void createMsg_PingClient (QByteArray *msg, int line);
    QLocalServer *server;
    QString socket_filename;

    int numClientConnected;
    quint64 cur_id;

    SSHMemPeerServer *peerList;
};

#endif // SSHMEMSERVER_H
