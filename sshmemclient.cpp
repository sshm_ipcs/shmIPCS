#include <math.h>
#include "sshmemclient.h"

/*  ___________________________________________________________________
 * |                                                                   |
 * |                     COSTRUCTOR / DESTRUCTOR                       |
 * |___________________________________________________________________|
 */
SSHMemClient::SSHMemClient(quint64 shm_size, bool usePolling, QThread *parent) :
                           QThread(parent), sshMem(shm_size, SHM_KEY, false) {
    this->socket = new QLocalSocket (this);
    this->is_initialized = false;
    this->is_shm_initialized = false;
    this->localPeer = new sshMemClientPeer;
    this->localPeer->message_port = new PosixMQ ();
    this->remotePeerList = new SSHMemPeerClient ();

    this->socket->setReadBufferSize((qint64)0);

    this->usePolling = usePolling;
    if ( this->usePolling == true ) {
        this->queue_message = new QQueue<queue_msg_element> ();
        this->queue_system  = new QQueue<queue_sys_element> ();
    }
}


SSHMemClient::SSHMemClient(QString sockFileName, quint64 shm_size, QString shm_key,  bool usePolling,
                           QThread *parent) : QThread(parent), sshMem(shm_size, shm_key, false) {
    this->socket = new QLocalSocket (this);
    this->is_initialized = false;
    this->is_shm_initialized = false;
    this->localPeer = new sshMemClientPeer;
    this->localPeer->message_port = new PosixMQ ();
    this->remotePeerList = new SSHMemPeerClient ();
    this->socket_filename = sockFileName;

    this->socket->setReadBufferSize((qint64)0);

    this->usePolling = usePolling;
}


void SSHMemClient::run () {
    QByteArray message;
    while (1) {
       if ( isClientConnected() ) {
            if ( this->localPeer->message_port->msg_to_read () > 0 ) {
                this->localPeer->message_port->receive (&message);
                this->analyzesMsg (message);
            }

           this->socket->waitForReadyRead(50);
            if ( socket->bytesAvailable() >=  (int)sizeof(quint16) * 2) {
              _readMessage ();
            }
        }

       msleep (100);
    }
}


SSHMemClient::~SSHMemClient () {
    if ( shm_initialized )
        this->sharedMemory->detach ();
}



/*  ___________________________________________________________________
 * |                                                                   |
 * |                      EVENT POLLING MANAGEMENT                     |
 * |___________________________________________________________________|
 */
int SSHMemClient::num_pending_message () {
    return this->queue_message->length ();
}


int SSHMemClient::num_pending_sys_msg () {
    return this->queue_system->length ();
}


int SSHMemClient::pop_message (queue_msg_element_t *msg) {
    if ( !this->queue_message->isEmpty() ) {
        *msg = (queue_msg_element_t)this->queue_message->dequeue ();
        return 1;
    } else {
        msg = NULL;
        return 0;
    }
}


int SSHMemClient::pop_sys_msg (queue_sys_element_t *msg) {
    if ( !this->queue_system->isEmpty() ) {
        *msg = (queue_sys_element_t)this->queue_system->dequeue ();
        return 1;
    } else {
        msg = NULL;
        return 0;
    }
}



/*  ___________________________________________________________________
 * |                                                                   |
 * |                        ATTRIBUTE INTERFACE                        |
 * |___________________________________________________________________|
 */
QString SSHMemClient::getSocketFileName () {
    return this->socket_filename;
}


int SSHMemClient::setSocketFileName (QString FileName) {
    if ( isClientConnected() ) {
        return -1;
    } else {
        socket_filename = FileName;
        return 0;
    }
}


bool SSHMemClient::isInitialized () {
    return is_initialized;
}


bool SSHMemClient::isShmInitialized () {
    return is_shm_initialized;
}

const sshMemClientPeer SSHMemClient::getLocalPeer () {
    return (const sshMemClientPeer)*this->localPeer;
}


const SSHMemPeerClient SSHMemClient::getRemotePeerList () {
    return (const SSHMemPeerClient)*this->remotePeerList;
}


quint64 SSHMemClient::getClientID () {
    if ( this->isClientConnected () )
        return this->localPeer->id;
    else
        return 0;
}


qintptr SSHMemClient::getSockID () {
    if ( this->isClientConnected () )
        return this->localPeer->sock_fd;
    else
        return 0;
}


/*  ___________________________________________________________________
 * |                                                                   |
 * |                          SOCKET MANAGEMENT                        |
 * |___________________________________________________________________|
 */
int SSHMemClient::ConnectClient () {
    if ( this->socket_filename.isEmpty() )
        return -1;

    this->socket->connectToServer (this->socket_filename, QIODevice::ReadWrite | QIODevice::Append);
    return  this->socket->waitForConnected (3000) == true ? 1 : 0;
}


int SSHMemClient::DisconnectClient () {
    this->socket->abort ();

    if ( shm_initialized )
        this->sharedMemory->detach ();

    return true;
}


int SSHMemClient::isClientConnected () {
    return socket->state() == QLocalSocket::ConnectedState ? 1 : 0;
}


bool SSHMemClient::dataReady () {
    if ( isClientConnected () == true ) {
        return this->socket->bytesAvailable() > (int)sizeof(quint16) * 2;
    } else {
        return false;
    }
}


bool SSHMemClient::isValidSockID (qintptr socked_id) {
    return socked_id > 0;
}


bool SSHMemClient::isValidClientID (quint64 client_id) {
    //return peer_id != 0 ? true : false;
    return true;  // temp
}


bool SSHMemClient::manageMsg_InitStage (QDataStream *in) {
    qintptr sock_id;
    quint64 client_id;

    if ( this->isInitialized () )
        return false;

    *in >> client_id;
    *in >> sock_id;

    if ( isValidClientID (client_id) && isValidSockID (sock_id) ) {
        this->localPeer->id = client_id;
        this->localPeer->sock_fd = sock_id;
        return true;
    } else
        return false;
}


bool SSHMemClient::manageMsg_InitPeer (QDataStream *in) {
    QString mq_name;

    if ( this->isInitialized () )
        return false;

    *in >> mq_name;
    this->localPeer->message_port->set_fd_name (mq_name);

    return true;
}


bool SSHMemClient::manageMsg_InfoPeer (QDataStream *in, sshMemClientPeer *other_peer) {
    qintptr sock_id;
    quint64 client_id;
    QString mq_name;

    *in >> client_id;
    *in >> sock_id;
    *in >> mq_name;

    SSHMemPeerClient::InitPeer (other_peer, sock_id, client_id);
    other_peer->message_port->set_fd_name (mq_name);

    return true;
}


bool SSHMemClient::manageMsg_NewClient (QDataStream *in, sshMemClientPeer *new_client ) {
    qintptr sock_id;
    quint64 client_id;
    QString mq_name;

    *in >> client_id;
    *in >> sock_id;
    *in >> mq_name;

    SSHMemPeerClient::InitPeer (new_client, sock_id, client_id);
    new_client->message_port->set_fd_name (mq_name);

    return true;
}


bool SSHMemClient::manageMsg_ShmKey (QDataStream *in) {
    QString key;

    *in >> key;
    if ( key.length() > 0 ) {
        this->setMemKey (key);
        return true;
    } else
        return false;
}



/*  ___________________________________________________________________
 * |                                                                   |
 * |                            SLOT HANDLER                           |
 * |___________________________________________________________________|
 */
void SSHMemClient::_readMessage () {
    int ret = 0;
    QByteArray msg;
    static quint16 blockSize = 0;
    static quint16 code = 0;
    sshMemClientPeer *new_client = NULL, *other_peer = NULL;
    queue_sys_element_t sys_msg;
    QDataStream in(socket);
    in.setVersion(QDataStream::Qt_4_0);

    while (1) {
        if (socket->bytesAvailable() < (int)sizeof(quint16) * 2)
            return;

        if ( code == 0 && blockSize == 0 ) {
            in >> code;
            in >> blockSize;
        }

        if ( socket->bytesAvailable() < blockSize )
            return;

        switch ( code ) {

        case MSG_SOCK_INIT: {
            bool status = manageMsg_InitStage (&in);
            if ( status )

                if ( this->usePolling ) {
                    sys_msg.data.clear ();
                    sys_msg.code = MSG_P_SOCK_INIT;
                    sys_msg.data.append (this->localPeer->id);
                    sys_msg.data.append (this->localPeer->sock_fd);
                    this->queue_system->append (sys_msg);
                } else {
                    emit clientInitialized (this->localPeer->id, this->localPeer->sock_fd);
                }

            break;
        }

        case MSG_SOCK_MQ_INFO: {
            bool status = manageMsg_InitPeer (&in);
            if ( status )
                ret = SSHMemPeerClient::open_msg_own_port (this->localPeer->message_port);
                if ( ret == 0 ) {
                    this->is_initialized = true;

                    if ( this->usePolling ) {
                        sys_msg.data.clear ();
                        sys_msg.code = MSG_P_SOCK_MQ_INFO;
                        sys_msg.data.append (
                                    QVariant(this->localPeer->message_port->get_fd_name()));
                        this->queue_system->append (sys_msg);
                    } else {
                        emit clientPeerInitialized
                                (this->localPeer->message_port->get_fd_name());
                    }

                } else {

                    if ( this->usePolling ) {
                        sys_msg.data.clear ();
                        sys_msg.code = MSG_P_SOCK_ERROR;
                        sys_msg.data.append (ret);
                        this->queue_system->append (sys_msg);
                    } else {
                        emit clientPeerConnectionError (ret);
                    }

                }
            break;
        }

        case MSG_SOCK_ADD_CLIENT: {
            other_peer = new sshMemClientPeer;
            bool status = manageMsg_InfoPeer (&in, other_peer);
            status &= (bool)!SSHMemPeerClient::open_msg_remote_port(other_peer->message_port);
            if ( status ) {
                this->remotePeerList->append (*other_peer);

                if ( this->usePolling ) {
                    sys_msg.data.clear ();
                    sys_msg.code = MSG_P_SOCK_ADD_CLIENT;
                    this->queue_system->append (sys_msg);
                } else {
                    emit clientUpdateInterconnection ();
                }

            }
            break;
        }

        case MSG_SOCK_NEW_CLIENT: {
            new_client = new sshMemClientPeer;
            bool status = manageMsg_NewClient (&in, new_client);
            status &= (bool)!SSHMemPeerClient::open_msg_remote_port(new_client->message_port);
            if ( status ) {
                this->remotePeerList->append (*new_client);

                if ( this->usePolling ) {
                    sys_msg.data.clear ();
                    sys_msg.code = MSG_P_SOCK_NEW_CLIENT;
                    sys_msg.data.append (new_client->sock_fd);
                    sys_msg.data.append (new_client->id);
                    this->queue_system->append (sys_msg);
                } else {
                    emit newClientConnected (new_client->sock_fd, new_client->id);
                }

            }
            break;
        }

        case MSG_SHMEM_KEY: {
            bool status = manageMsg_ShmKey (&in);
            if ( status ) {
                ret = attachSHM ();
                if ( ret == 0 ) {
                    this->is_shm_initialized = true;

                    if ( this->usePolling ) {
                        sys_msg.data.clear ();
                        sys_msg.code = MSG_P_SHMEM_KEY;
                        sys_msg.data.append (getMemKey());
                        sys_msg.data.append ((int)getMemSize());
                        this->queue_system->append (sys_msg);
                    } else {
                        emit sharedMemoryAttached (getMemKey(), (int)getMemSize());
                    }

                } else {

                    if ( this->usePolling ) {
                        sys_msg.data.clear ();
                        sys_msg.code = MSG_P_MEM_ATTACH_ERROR;
                        sys_msg.data.append (ret);
                        this->queue_system->append (sys_msg);
                    } else {
                        emit sharedMemoryAttachError (ret);
                    }

                }
            }
            break;
        }

        default:
            in >> msg;

            if ( this->usePolling ) {
                sys_msg.code = MSG_P_GENERIC;
                sys_msg.data.append (msg.data());
                this->queue_system->append (sys_msg);
            } else {
                emit newMessage (msg.data());
            }

            break;
        }

        code = 0;
        blockSize = 0;

        if ( socket->bytesAvailable() == 0 )
            break;
    }

}



/*  ___________________________________________________________________
 * |                                                                   |
 * |                      PEER MANAGEMENT (VIA MQ)                     |
 * |___________________________________________________________________|
 */
int SSHMemClient::analyzesMsg (QByteArray msg) {
    qint8 code = 0;

    if ( msg.length() < MQ_CODE_SIZE )
        return -EINVAL;

    code = (qint8)msg.at(0);
    msg.remove(0, 1);

    switch ( code ) {
    case MSG_PEER_PING_S:
        this->manageMsg_ReceivedPingServer (msg);
        break;
    case MSG_PEER_PING_C:
        this->manageMsg_ReceivedPingClient (msg);
        break;
    default:
        return -EINVAL;
    }

}


int SSHMemClient::manageMsg_ReceivedPingServer (QByteArray msg) {
    qint64              line_code = 0;
    queue_msg_element_t pmsg;
    QDataStream         data(&msg, QIODevice::ReadOnly);
    data.setVersion (QDataStream::Qt_4_0);

    data >> line_code;
    if ( line_code > MAX_INT_LINE )
        return -EINVAL;

    if ( this->usePolling ) {
        pmsg.source_id = 0;
        pmsg.line = line_code;
        this->queue_message->append (pmsg);
    } else {
        emit PingReceived (0, line_code);   // 0 is for the Server

    }

    return 0;
}


int SSHMemClient::manageMsg_ReceivedPingClient (QByteArray msg) {
    qint64              line_code = 0;
    qint64              clientID  = 0;
    queue_msg_element_t pmsg;
    QDataStream         data(&msg, QIODevice::ReadOnly);
    data.setVersion (QDataStream::Qt_4_0);

    data >> clientID;
    data >> line_code;
    if ( line_code > MAX_INT_LINE )
        return -EINVAL;

    if ( this->usePolling ) {
        pmsg.source_id = clientID;
        pmsg.line = line_code;
        this->queue_message->append (pmsg);
    } else {
        emit PingReceived (clientID, line_code);   // 0 is for the Server

    }

    return 0;
}


void SSHMemClient::createMsg_PingClient (QByteArray *msg, int line)
{
    QDataStream data(msg, QIODevice::WriteOnly);
    data.setVersion (QDataStream::Qt_4_0);

    data << (qint8)MSG_PEER_PING_C;
    data << (qint64)this->localPeer->id;
    data << (qint64)(line);
}


int SSHMemClient::send_PingToClient (int clientID, int line) {
    sshMemClientPeer   *client_peer = NULL;
    int                ret          = 0;
    QByteArray         msg;

    if ( line > MAX_INT_LINE)
        return -EINVAL;

    if ( !this->isClientConnected() )
        return -1;

    client_peer = (sshMemClientPeer *)this->remotePeerList->searchPeer (clientID);
    if ( !client_peer )
        return -1;

    this->createMsg_PingClient (&msg, line);
    ret = client_peer->message_port->send (msg);
    return ret;
}

